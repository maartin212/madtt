const cheerio = require("cheerio");
const axios = require("axios");

const originarUrl =
  "https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-0-origin.html";
const case1Url =
  "https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-1-evil-gemini.html";
const case2Url =
  "https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-2-container-and-clone.html";
const case3Url =
  "https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-3-the-escape.html";
const case4Url =
  "https://agileengine.bitbucket.io/beKIvpUlPMtzhfAy/samples/sample-4-the-mash.html";

const urls = [case1Url, case2Url, case3Url, case4Url];

const getOriginalButton = async () => {
  const res = await axios.get(originarUrl);
  const $ = cheerio.load(res.data);
  return $("#make-everything-ok-button");
};

const findEl = async (url, tag, text, attr) => {
  const res = await axios.get(url);
  const $ = cheerio.load(res.data);
  const but = $(tag).filter(`.${attr.class.split(" ")[0]}`);

  const firstFilter = $(but).filter(`.${attr.class.split(" ")[1]}`); // 4 3
  if (firstFilter.length === 1) return firstFilter;

  const secondFilter = $(but).filter((i, el) => $(el).text() === text); // 1
  if (secondFilter.length === 1) return secondFilter;

  const thirdFilter = $(but).filter(
    (i, el) => $(el).attr("href") === attr.href
  ); // 2 4
  if (thirdFilter.length === 1) return thirdFilter;

  return null;
};

const index = async () => {
  const originalBtn = await getOriginalButton();
  const originalBtnTag = originalBtn.get(0).tagName;
  const originalBtnText = originalBtn.text();
  const originalBtnAttr = originalBtn.attr();

  const searchBtns = urls.map((url, index) =>
    findEl(url, originalBtnTag, originalBtnText, originalBtnAttr)
  );

  Promise.all(searchBtns).then(e =>
    e.forEach((el, index) => {
      let node = el.parent();
      const routeArr = [];

      routeArr.push(`${el.get(0).tagName} ( ${el.text().trim()} )`);
      while (node.get(0).tagName !== "body") {
        const nodeTag = node.get(0).tagName;
        routeArr.push(`${nodeTag}[${node.index()}]`);
        node = node.parent();
      }
      routeArr.push(node.get(0).tagName);

      const route = routeArr.reverse().join(" > ");
      console.log(`case${index + 1}`, route);
    })
  );
};

index();
